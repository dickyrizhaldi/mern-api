const express = require('express');

const router = express.Router();

const productController = require('../controllers/product');

// POST -> CREATE 
router.post('/product', productController.createProduct);


// GET -> READ 
router.get('/products', productController.getAllProduct);



module.exports = router;
